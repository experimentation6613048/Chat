using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System;



public class ChatArea : TableLayoutPanel {
    

    public ChatArea() {

        //BackColor = Color.Blue;

        Dock = DockStyle.Fill;
        AutoScroll = true;

        BorderStyle = BorderStyle.None;
        CellBorderStyle = TableLayoutPanelCellBorderStyle.None;


        RowCount = 0;
    }

    public void AddMessage(string message, string user) {
        if(RowCount > 0) {
            Chat last = (Chat)GetControlFromPosition(0,RowCount-1);
            
            if(user == last.proprietaire) {
                last.extendMessage(message);
                
                RowStyle restyle = new RowStyle();
                restyle.SizeType = SizeType.Absolute;
                restyle.Height = (int)(last.message.Height*1.2);
                this.RowStyles[RowCount - 1] = restyle;

                return;
            }
        }

        Chat chat = new Chat(message, user);
        RowCount++;

        this.Controls.Add(chat, 0, RowCount - 1);

        RowStyle style = new RowStyle();
        style.SizeType = SizeType.Absolute;
        style.Height = (int)(chat.message.Height*1.2);
        this.RowStyles.Add(style);
    }


}