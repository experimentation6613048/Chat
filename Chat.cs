using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;


public class Chat : Panel {
    public Label message;
    public string proprietaire;

    public Chat(string message, string user) {
        //BackColor = Color.Red;


        BorderStyle = BorderStyle.None;
        proprietaire = user;

        this.message = new Label();
        this.message.BackColor = Color.Transparent;

        if(user == "Moi") {
            this.message.TextAlign = ContentAlignment.TopLeft;
            Dock = DockStyle.Right;
        }
        else {
            
            this.message.TextAlign = ContentAlignment.TopLeft;
            Dock = DockStyle.Left;
        }

        this.message.Text = message;
        this.Controls.Add(this.message);
        
        this.message.AutoSize = true;

        Size = this.message.Size;

        this.message.BorderStyle = BorderStyle.None;

        



        GraphicsPath path = new GraphicsPath();



        Bitmap bitmap = new Bitmap(Width, Height);

        Graphics graphics = Graphics.FromImage(bitmap);


        int cornerRadius = 20;
        Rectangle rect = new Rectangle(0, 0, (int)(Width*1.2), (int)(Height*1.2));

        path.AddArc(rect.X, rect.Y, cornerRadius, cornerRadius, 180, 90);
        path.AddArc(rect.X + rect.Width - cornerRadius, rect.Y, cornerRadius, cornerRadius, 270, 90);
        path.AddArc(rect.X + rect.Width - cornerRadius, rect.Y + rect.Height - cornerRadius, cornerRadius, cornerRadius, 0, 90);
        path.AddArc(rect.X, rect.Y + rect.Height - cornerRadius, cornerRadius, cornerRadius, 90, 90);

        path.CloseAllFigures();

        LinearGradientBrush brush = new LinearGradientBrush(new Point(0, 0), new Point(0, 100), Color.FromArgb(41, 138, 10 ), Color.FromArgb(105, 200, 75));
        graphics.FillRectangle(brush, 0, 0, 100, 100);


        BackgroundImage = bitmap;

    }

    public void extendMessage(string message) {
        this.message.Text += "\n" + message;

        GraphicsPath path = new GraphicsPath();



        Bitmap bitmap = new Bitmap(Width, Height);

        Graphics graphics = Graphics.FromImage(bitmap);


        int cornerRadius = 20;
        Rectangle rect = new Rectangle(0, 0, (int)(Width*1.2), (int)(Height*1.2));

        path.AddArc(rect.X, rect.Y, cornerRadius, cornerRadius, 180, 90);
        path.AddArc(rect.X + rect.Width - cornerRadius, rect.Y, cornerRadius, cornerRadius, 270, 90);
        path.AddArc(rect.X + rect.Width - cornerRadius, rect.Y + rect.Height - cornerRadius, cornerRadius, cornerRadius, 0, 90);
        path.AddArc(rect.X, rect.Y + rect.Height - cornerRadius, cornerRadius, cornerRadius, 90, 90);

        path.CloseAllFigures();

        LinearGradientBrush brush = new LinearGradientBrush(new Point(0, 0), new Point(0, 100), Color.FromArgb(41, 138, 10 ), Color.FromArgb(105, 200, 75));
        graphics.FillRectangle(brush, 0, 0, 100, 100);


        BackgroundImage = bitmap;
    }
    

}