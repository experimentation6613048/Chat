using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ClientApp
{
    public partial class MainForm : Form
    {
        private TcpClient client;
        private StreamReader reader;
        private StreamWriter writer;
        private Thread readThread;

        private ChatArea chats;
        private TextBox messageTextBox;
        private Button sendButton;
        

        public MainForm()
        {
            this.Text = "Chat Client";
            this.Width = 300;
            this.Height = 400;

            // Créer un TableLayoutPanel avec 2 colonnes et 2 lignes
            TableLayoutPanel ChatPanel = new TableLayoutPanel();
            ChatPanel.Width = (int)(this.Width*0.9);
            ChatPanel.Height = (int)(this.Height*0.9);
            ChatPanel.ColumnCount = 2;
            ChatPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 80F));
            ChatPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            ChatPanel.RowCount = 2;
            ChatPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 80F));
            ChatPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 20F));
            this.Controls.Add(ChatPanel);

            // Ajouter les contrôles au TableLayoutPanel
            messageTextBox = new TextBox();
            messageTextBox.Multiline = true;
            messageTextBox.Dock = DockStyle.Fill;
            ChatPanel.Controls.Add(messageTextBox, 1, 0);
            

            chats = new ChatArea();
            ChatPanel.Controls.Add(chats, 0, 0);
            ChatPanel.SetColumnSpan(chats, 2);
            chats.Dock = DockStyle.Fill;


            sendButton = new Button();
            sendButton.Text = "Send";
            sendButton.Dock = DockStyle.Fill;
            sendButton.Click += new EventHandler(sendButton_Click);
            ChatPanel.Controls.Add(sendButton, 1, 1);

        }

        public void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                client = new TcpClient("localhost", 8888);
                reader = new StreamReader(client.GetStream());
                writer = new StreamWriter(client.GetStream());

                readThread = new Thread(new ThreadStart(ReceiveMessages));
                readThread.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Disconnect();
        }

        private void Disconnect()
        {
            try
            {
                readThread.Abort();
                reader.Close();
                writer.Close();
                client.Close();
            }
            catch { }
        }

        private void ReceiveMessages()
        {
            while (true)
            {
                try
                {

                    string message = reader.ReadLine();
                    if(message != null) {
                        Invoke(new MethodInvoker(delegate {
                            chats.AddMessage(message, "Other");
                        }));
                    }
                    
                }
                catch (IOException)
                {
                    break;
                }
            }
        }



        private void sendButton_Click(object sender, EventArgs e)
        {
            string message = messageTextBox.Text;
            if(message != "")
            {
                writer.WriteLine(message);
                chats.AddMessage(message, "Moi");
                writer.Flush();
                messageTextBox.Clear();
            }
        }
    }
}
