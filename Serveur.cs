using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ServerApp
{
    class Server
    {
        private readonly TcpListener server;
        private readonly List<TcpClient> clientList = new List<TcpClient>();

        public Server(int port)
        {
            server = new TcpListener(IPAddress.Any, port);
        }

        public void StartServer()
        {
            server.Start();
            Console.WriteLine("Server started and listening on port " + ((IPEndPoint)server.LocalEndpoint).Port);
            Thread clientThread = new Thread(new ThreadStart(AcceptClients));
            clientThread.Start();
        }

        private void AcceptClients()
        {
            while (true)
            {
                TcpClient client = server.AcceptTcpClient();
                clientList.Add(client);
                Console.WriteLine("Client connected: " + ((IPEndPoint)client.Client.RemoteEndPoint).Address.ToString());

                Thread clientThread = new Thread(new ParameterizedThreadStart(ClientHandler));
                clientThread.Start(client);
            }
        }

        private void ClientHandler(object clientObj)
        {
            TcpClient client = (TcpClient)clientObj;
            NetworkStream stream = client.GetStream();

            while (true)
            {
                try
                {
                    byte[] buffer = new byte[1024];
                    int bytesRead = stream.Read(buffer, 0, buffer.Length);
                    if (bytesRead == 0)
                    {
                        break;
                    }

                    string message = Encoding.ASCII.GetString(buffer, 0, bytesRead);
                    Console.WriteLine("Received message: " + message);

                    Broadcast(message, client);
                }
                catch (IOException)
                {
                    break;
                }
            }

            clientList.Remove(client);
            Console.WriteLine("Client disconnected: " + ((IPEndPoint)client.Client.RemoteEndPoint).Address.ToString());
        }

        private void Broadcast(string message, TcpClient excludeClient = null)
        {
            byte[] buffer = Encoding.ASCII.GetBytes(message);

            foreach (TcpClient client in clientList)
            {
                if (client != excludeClient)
                {
                    NetworkStream clientStream = client.GetStream();
                    clientStream.Write(buffer, 0, buffer.Length);
                    clientStream.Flush();
                }
            }
        }

        static void Main(string[] args)
        {
            Server server = new Server(8888);
            server.StartServer();
        }
    }
}
